<?php

namespace C33s\Robo\Task\Symfony;

use C33s\Robo\Task\ExecPhp\ExecPhp;

class ExecSymfonyTask extends ExecPhp
{
    protected static $defaultEnvironment = null;

    /**
     * sets the Environment string which should be used as default.
     *
     * @param string $defaultEnvironment
     */
    public static function setDefaultEnvironment($defaultEnvironment)
    {
        self::$defaultEnvironment = $defaultEnvironment;
    }

    /**
     * Gets the default Environment.
     *
     * @return string
     */
    public static function getDefaultEnvironment()
    {
        return self::$defaultEnvironment;
    }

    /**
     * Builds the env string from the default environment.
     *
     * @return string
     */
    protected function buildEnvironmentString()
    {
        if (!self::$defaultEnvironment) {
            return '';
        }

        if (!is_string(self::$defaultEnvironment)) {
            throw new \InvalidArgumentException('invalid type for defaultEnvironment given');
        }

        return ' --env='.self::$defaultEnvironment;
    }

    public function getCommand()
    {
        $command = parent::getCommand();
        if (false === stripos($command, '--env')) {
            $command .= $this->buildEnvironmentString();
        }

        return $command;
    }
}
