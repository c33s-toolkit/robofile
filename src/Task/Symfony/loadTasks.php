<?php

namespace C33s\Robo\Task\Symfony;

use Consolidation\AnnotatedCommand\CommandData;
use Robo\Contract\CommandInterface;
use Robo\Result;

trait loadTasks
{
    protected function taskExecSymfony($command)
    {
        return $this->task(ExecSymfonyTask::class, $command);
    }

    /**
     * @param string|CommandInterface $command
     * @param array                   $mergeEnvironment
     * @param null                    $environment
     * @param bool                    $enableXDebug
     *
     * @return Result
     */
    protected function _execSymfony($command, $mergeEnvironment = [], $environment = null, $enableXDebug = false)
    {
        $task = $this->taskExecSymfony($command);
        if (!empty($mergeEnvironment)) {
            if (!$environment) {
                $environment = $_ENV;
            }
            $task->envVars(array_merge($mergeEnvironment, $environment));
        }
        if ($enableXDebug) {
            $task->enableXDebug($enableXDebug);
        }

        return $task->run();
    }

    /**
     * alias for _execSymfony.
     *
     * @see _execSymfony()
     */
    protected function _execSf($command, $mergeEnvironment = [], $environment = null, $enableXDebug = false)
    {
        $this->_execSymfony($command, $mergeEnvironment, $environment, $enableXDebug);
    }

    /**
     * alias for _execSymfony.
     *
     * @see _execSymfony()
     */
    protected function _runConsole($command, $mergeEnvironment = [], $environment = null, $enableXDebug = false)
    {
        $this->_execSymfony($command, $mergeEnvironment, $environment, $enableXDebug);
    }

    /**
     * Hook to check for the env option and set the option as default environment in the ExecSymfonyTask.
     *
     * @hook pre-command
     */
    public function setDefaultSymfonyEnvironment(CommandData $commandData)
    {
        $input = $commandData->input();

        if ($input->hasOption('env') && $input->getOption('env')) {
            ExecSymfonyTask::setDefaultEnvironment($input->getOption('env'));
        }
    }
}
