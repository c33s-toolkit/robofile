<?php

namespace C33s\Robo\Task\Extra;

use Version\Version;

trait VersionTasks
{
    protected $versionFile = 'VERSION';

    /**
     * Updates the `VERSION` file.
     */
    public function versionUpdate()
    {
        $buildCount = 1;
        $this->_touch($this->versionFile);
        $currentVersionString = file_get_contents($this->versionFile);
        if (empty($currentVersionString)) {
            $currentVersionString = '0.1.0';
        }
        $currentVersion = Version::fromString($currentVersionString);
        $commitHashShort = $this->getCurrentCommitHash(true);
        $gitTagLast = $this->getLastTag();
        $gitTag = $this->getTagIfOnTag();

        $newVersion = clone $currentVersion;
        if ($gitTag) {
            $gitVersion = Version::fromString($gitTag);
            $newVersion = clone $gitVersion;
        }

        if ($gitTagLast) {
            $gitTagLastVersion = Version::fromString($gitTagLast);
            $gitTagLastVersion = $gitTagLastVersion->withPreRelease([]);
            $newVersion = clone $gitTagLastVersion;
            $gitTagLastParts = explode('-', $gitTagLast);
            if (array_key_exists(1, $gitTagLastParts)) {
                $buildCount = $gitTagLastParts[1] + 1;
            }
        }

        $buildContent = str_pad($buildCount, 3, '0', STR_PAD_LEFT).'.'.$commitHashShort;
        $ciPipelineId = $this->getEnv('CI_PIPELINE_ID', null);
        if ($ciPipelineId) {
            $buildContent = $buildContent.'.'.$ciPipelineId;
        }
        $ciJobId = $this->getEnv('CI_JOB_ID', null);
        if ($ciJobId) {
            $buildContent = $buildContent.'.'.$ciJobId;
        }

        $newVersion = $newVersion->withBuild($buildContent);
        file_put_contents($this->versionFile, $newVersion);
        $this->writeln($newVersion->getVersionString());
    }

    /**
     * Gets the current commit hash.
     *
     * @param bool $short
     */
    protected function getCurrentCommitHash($short = true)
    {
        $short = ($short) ? '--short' : '';
        $execResult = $this->_exec("git rev-parse {$short} HEAD || true");
        $commitHash = trim($execResult->getMessage());

        return $commitHash;
    }

    /**
     * returns the tag `1.2.0` or the tag with the diff count to the next tag `1.2.0-2-hash`.
     */
    protected function getLastTag()
    {
        $execResult = $this->_exec('git describe --tags || true');
        $gitTagLast = trim($execResult->getMessage());

        return $this->cleanupTag($gitTagLast);
    }

    /**
     * returns the tag `1.2.0` or is empty if the current HEAD is not tagged.
     */
    protected function getTagIfOnTag()
    {
        $execResult = $this->_exec('git --no-pager tag --points-at HEAD || true');
        $gitTag = trim($execResult->getMessage());

        return $this->cleanupTag($gitTag);
    }

    /**
     * @param string $gitTag
     *
     * @return string
     */
    protected function cleanupTag($gitTag)
    {
        return ltrim($gitTag, 'v');
    }
}
