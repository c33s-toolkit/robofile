<?php

namespace C33s\Robo\Task\Extra;

trait AssetsTasks
{
    /**
     * Update yarn packages and build dev assets.
     */
    public function assetsUpdate() //TODO: php7 - : void
    {
        $this->_exec('yarn install');
        $this->assetsInstall();
    }

    /**
     * Build/Compile Encore assets.
     */
    public function assetsInstall() //TODO: php7 -: void
    {
        $environment = 'dev';
        if ($this->isEnvironmentProduction()) {
            $environment = 'production';
        }
        $this->_exec("yarn run encore $environment");
    }

    /**
     * Start Encore Assets Server (not for production server!).
     */
    public function assetsServer() //TODO: php7 -: void
    {
        if ($this->isEnvironmentProduction()) {
            if (!$this->confirm('YOU MUST NOT USE THIS ON THE PRODUCTION SERVER! CONTINUE?')) {
                $this->abort();
            }
        }
        $this->_exec('yarn dev-server');
    }
}
