<?php

namespace C33s\Robo\Task\Extra;

trait CheckTasks
{
    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->checkCs($arguments);
        $this->checkCsfixer($arguments);
        $this->checkPhpstan($arguments);
//        $this->checkTwigcs($arguments);
        $this->checkRequire($arguments);
        $this->checkUnused($arguments);

//        $this->_execPhpQuiet('php bin/console lint:yaml config --parse-tags');
//        $this->_execPhpQuiet('php bin/console lint:twig templates --env=prod');
//        $this->_execPhpQuiet('php bin/console lint:container');
//        //$this->_execPhpQuiet('php bin/console lint:xliff translations');
//        //$this->_execPhpQuiet('php bin/console security:check');
//        $this->_execPhpQuiet('php bin/console doctrine:schema:validate --skip-sync -vvv --no-interaction');
    }

    public function checkCs($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/vendor/squizlabs/php_codesniffer/bin/phpcs $arguments");
    }

    public function checkCsFixer($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    public function checkStan($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/vendor/phpstan/phpstan/phpstan analyse -c phpstan.neon $arguments");
    }

    /**
     * @deprecated
     */
    public function checkPhpstan($arguments = '')
    {
        $this->checkStan($arguments);
    }

    /**
     * Lower versions (2.0) of require checker have parsing errors and higher versions (3.2) require php 7.4.
     * Switch for supporting multiple php versions.
     * <code>
     *  $this->_prepareCiModules([
     *       'composer-require-checker' => PHP_VERSION_ID < 70400 ? '2.0.0' : '3.2.0',
     *   ]);
     * </code>.
     */
    public function checkRequire($arguments = '')
    {
        if (PHP_VERSION_ID < 70400) {
            $arguments .= ' --ignore-parse-errors';
        }
        $this->_execPhpQuiet("php {$this->dir()}/bin/composer-require-checker.phar --ansi $arguments");
    }

    public function checkTwigCs($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/vendor/friendsoftwig/twigcs/bin/twigcs $arguments");
    }

    public function checkUnused($arguments = '')
    {
        $this->_execPhpQuiet("php {$this->dir()}/bin/composer-unused.phar --ansi $arguments");
    }
}
