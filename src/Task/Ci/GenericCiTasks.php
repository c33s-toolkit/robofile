<?php

namespace C33s\Robo\Task\Ci;

use C33s\Robo\Task\CiProvider\DownloadModules;
use C33s\Robo\Task\ExecPhp\ExecPhp;

trait GenericCiTasks
{
    /**
     * @param string $command
     *
     * @return ExecPhp
     */
    abstract protected function taskExecPhp($command);

    /**
     * @param array $modules
     *
     * @return DownloadModules
     */
    abstract protected function taskCiProviderDownloadModules($modules);

    //TODO: php7 - array;

    protected $defaultPorts = [
        'http' => 80,
        'https' => 443,
        'mysql' => 3306,
        'postgres' => 5432,
        'postgresql' => 5432,
        'elasticsearch' => 9200,
        'mongodb' => 27017,
    ];

    /**
     * Check if the given ports are in use, indicating a running service. For each service
     * to test, you MUST specify a name and MAY specify a port number and hostname.
     *
     * Examples:
     * [
     *     'name' => 'host:port',
     *     'name' => 1234, (defaults to localhost:1234)
     *     'name' => null, (defaults to anything defined for "name" in $this->defaultPorts)
     * ]
     *
     * @param array $portDefinitions
     *
     * @return bool True if all checks were successful
     */
    protected function ciCheckPorts($portDefinitions) //TODO: php7 - array
    {
        $notFound = [];
        foreach ($portDefinitions as $name => $settings) {
            if (empty($settings) && isset($this->defaultPorts[$name])) {
                $settings = $this->defaultPorts[$name];
            }
            if (is_int($settings)) {
                $settings = "localhost:$settings";
            }

            list($host, $port) = explode(':', $settings, 2);
            $host = $this->_resolveSymfonyParameter($host);
            $port = $this->_resolveSymfonyParameter($port);

            $this->output()->write("Checking port <comment>$host</comment>:<comment>$port</comment> for service <info>$name</info> ...");
            if ($this->isPortUsed($port, $host)) {
                $this->writeln(' <fg=white;bg=green;options=bold>CONNECT OK</fg=white;bg=green;options=bold>');
            } else {
                $notFound[] = "\n  * $name (<comment>$host</comment>:<comment>$port</comment>)";
                $this->writeln(' <fg=white;bg=red;options=bold>NOT RESPONDING</fg=white;bg=red;options=bold>');
            }
        }

        if (count($notFound) > 0) {
            $list = implode('', $notFound);
            $this->writeln("<error>The following services are not responding:</error>$list");

            return false;
        }

        return true;
    }

    /**
     * Update robo vendors in .robo directory by running composer update.
     */
    public function ciSelfUpdate()
    {
        $this
            ->taskCiProviderDownloadModules([
                'composer' => '@latest',
            ])
            ->run()
        ;
        $this->say('cd '.$this->dir().' && composer update && cd -');
        $this
            ->taskExecPhp('php bin/composer.phar update --no-interaction')
            ->dir($this->dir())
            ->run()
        ;
    }

    /**
     * Alias for ciSelfUpdate.
     */
    public function ciUpdate()
    {
        $this->ciSelfUpdate();
    }

    /**
     * adds the required .robo folders to the projects `.gitignore` file.
     */
    public function ciGitignore()
    {
        $dir = $this->dir();
        $newContent = <<< END
###> robo/cmd_ci:ignore ###
/{$dir}/bin
/{$dir}/cache
/{$dir}/vendor
###< robo/cmd_ci:ignore ###
END;
        $pattern = '/'.preg_quote($dir, '/').'/';
        $this->taskWriteToFile('.gitignore')
            ->append()
            ->appendUnlessMatches($pattern, $newContent)
            ->run()
        ;
    }
}
