<?php

namespace C33s\Robo\Task\ExecPhp;

use Robo\Exception\TaskException;
use Robo\Task\Base\Exec;
use Symfony\Component\Process\PhpExecutableFinder;

class ExecPhp extends Exec
{
    private $enableXDebug = false;

    /**
     * Enable XDebug for this call.
     *
     * @param bool $enableXDebug
     *
     * @return $this
     */
    public function enableXDebug($enableXDebug = true)
    {
        $this->enableXDebug = (bool) $enableXDebug;

        return $this;
    }

    /**
     * @return string
     *
     * @throws TaskException
     */
    protected function findPhp()
    {
        $php = (new PhpExecutableFinder())->find();
        if (false === $php) {
            throw new TaskException($this, 'Cannot detect PHP executable. This should never happen in CLI environments.');
        }
        $this->printTaskDebug("Found PHP executable: $php");

        return $php;
    }

    protected function addXDebug()
    {
        if (!$this->enableXDebug || extension_loaded('xdebug')) {
            return '';
        }

        $xDebug = getenv('PHP_XDEBUG_EXTENSION_PATH');
        if (empty($xDebug)) {
            throw new TaskException(
                $this,
                <<<EOF
This task requires xdebug. Please either:
 * enable xdebug in your PHP CLI configuration or
 * provide the path to your xdebug.so/.dll using the 
   PHP_XDEBUG_EXTENSION_PATH environment variable:
   export PHP_XDEBUG_EXTENSION_PATH=/path/to/xdebug.so
EOF
);
        }

        return '-d zend_extension='.escapeshellarg($xDebug);
    }

    /**
     * Get command without PHP executable.
     *
     * @return string
     */
    protected function getStrippedCommand()
    {
        $command = parent::getCommand();
        if (0 === strpos($command, 'php ')) {
            $command = substr($command, 4);
        }

        return $command;
    }

    public function getCommand()
    {
        $command = trim($this->findPhp().' '.$this->addXDebug()).' '.$this->getStrippedCommand();

        return $command;
    }
}
