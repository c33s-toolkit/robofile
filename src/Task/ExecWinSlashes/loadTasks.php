<?php

namespace C33s\Robo\Task\ExecWinSlashes;

trait loadTasks
{
    /**
     * Overwriting exec task to use automatic windows slash replacement.
     *
     * @param string|\Robo\Contract\CommandInterface $command
     *
     * @return ExecWinSlashes
     */
    protected function taskExec($command)
    {
        return $this->task(ExecWinSlashes::class, $command);
    }
}
