<?php

namespace C33s\Robo\Task\CiProvider;

use Robo\Common\BuilderAwareTrait;
use Robo\Contract\BuilderAwareInterface;
use Robo\Exception\TaskException;
use Robo\Result;
use Robo\Task\BaseTask;

/**
 * @method \RoboFile collectionBuilder()
 */
class FetchModulesFile extends BaseTask implements BuilderAwareInterface
{
    use BuilderAwareTrait;

    private $providerUri = 'https://robo.c33s.services/';
    private $modulesFile = C33S_ROBO_DIR.'/cache/ci-provider.json';

    /**
     * Set a custom CI provider URI.
     *
     * @param string $providerUri
     *
     * @return FetchModulesFile
     */
    public function providerUri($providerUri) //TODO: php7 - string //TODO: php7 - string : self
    {
        $this->providerUri = $providerUri;

        return $this;
    }

    /**
     * Set the filename.
     *
     * @param string $modulesFile
     *
     * @return FetchModulesFile
     */
    public function modulesFile($modulesFile) //TODO: php7 - string //TODO: php7 -  self
    {
        $this->modulesFile = $modulesFile;

        return $this;
    }

    /**
     * @return Result
     *
     * @throws TaskException
     */
    public function run()
    {
        $this->printTaskInfo("Fetching latest CI provider modules list from {$this->providerUri}");
        $this->collectionBuilder()
            ->taskFileDownload($this->providerUri)
            ->toFile($this->modulesFile)
            ->run()
        ;

        return new Result($this, 0, 'Successfully fetched latest ci provider modules list');
    }
}
