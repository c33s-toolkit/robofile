<?php

namespace C33s\Robo\Task\CiProvider;

use Robo\Common\BuilderAwareTrait;
use Robo\Contract\BuilderAwareInterface;
use Robo\Exception\TaskException;
use Robo\Result;
use Robo\Task\BaseTask;
use Webmozart\Assert\Assert;

/**
 * @method \RoboFile collectionBuilder()
 */
class DownloadModules extends BaseTask implements BuilderAwareInterface
{
    use BuilderAwareTrait;

    /**
     * @var array
     */
    private $modules;

    /**
     * @var bool
     */
    private $forceRefreshList = false;

    /**
     * @var bool
     */
    private $listAlreadyRefreshed = false;

    /**
     * @var bool[]
     */
    private $filesRefreshed = [];

    /**
     * @var string
     */
    private $availableModulesFile = C33S_ROBO_DIR.'/cache/ci-provider.json';

    /**
     * @var string
     */
    private $binDir = C33S_ROBO_DIR.'/bin';

    public function __construct(array $modules)
    {
        $this->modules = $modules;
    }

    /**
     * Set the filename.
     *
     * @param string $availableModulesFile
     *
     * @return $this
     */
    public function availableModulesFile($availableModulesFile) //TODO: php7 - string
    {
        $this->availableModulesFile = $availableModulesFile;

        return $this;
    }

    /**
     * @return bool
     *
     * @throws TaskException
     */
    private function checkModules()
    {
        $message = 'Checking ci modules: ';
        foreach ($this->modules as $name => $version) {
            $message .= "<info>$name</info> <comment>$version</comment>, ";
        }
        $message = rtrim($message, ', ');

        $this->printTaskInfo($message);
        $availableModules = $this->getAvailableModules();
        foreach ($this->modules as $name => $version) {
            $this->printTaskDebug("  Checking module <info>$name</info> for version <info>$version</info>");

            if (!$this->checkModuleName($name, $availableModules)) {
                return $this->checkModules();
            }

            $availableVersions = $availableModules[$name]['versions'];
            if ('@latest' === $version && count($availableVersions) > 0) {
                $keys = array_keys($availableVersions);
                $version = end($keys);

                $this->printTaskDebug("    Latest version of module <info>$name</info> is <info>$version</info>");
            }
            if (!$this->checkModuleVersion($name, $version, $availableVersions)) {
                return $this->checkModules();
            }

            $versionData = $availableVersions[$version];
            if (!$this->checkModuleFile($availableModules[$name], $versionData)) {
                return $this->checkModules();
            }

            $this->createBatFile($availableModules[$name]['filename']);
        }

        return true;
    }

    /**
     * @param string $name
     *
     * @return bool
     *
     * @throws TaskException
     */
    private function checkModuleName($name, array $availableModules)
    {
        if (isset($availableModules[$name])) {
            $this->printTaskDebug("    Module <info>$name</info> exists");

            return true;
        }

        if ($this->listAlreadyRefreshed) {
            throw new TaskException($this, "Cannot find module $name in modules list");
        }
        $this->printTaskWarning("Module $name not found, trying to re-download modules list");
        $this->fetchAvailableModulesFile();

        return false;
    }

    /**
     * @param string $name
     * @param string $version
     *
     * @return bool
     *
     * @throws TaskException
     */
    private function checkModuleVersion($name, $version, array $availableVersions)
    {
        if (isset($availableVersions[$version])) {
            $this->printTaskDebug("    Module <info>$name</info> exists in version <info>$version</info>");

            return true;
        }

        if ($this->listAlreadyRefreshed) {
            $available = implode(', ', array_keys($availableVersions));
            throw new TaskException($this, "Cannot find module $name in version $version in modules list. Available versions are $available");
        }
        $this->printTaskWarning("Module $name not found in version $version, trying to re-download modules list");
        $this->fetchAvailableModulesFile();

        return false;
    }

    /**
     * @param array $moduleData
     * @param array $versionData
     *
     * @return bool
     *
     * @throws TaskException
     */
    private function checkModuleFile($moduleData, $versionData) //TODO: php7 - array array
    {
        $file = $this->binDir.'/'.$moduleData['filename'];
        if (!file_exists($file)) {
            $this->printTaskDebug("    Module file <info>$file</info> does not exist, downloading from <info>{$versionData['url']}</info>");

            $this->collectionBuilder()
                ->taskFileDownload($versionData['url'])
                ->toFile($file)
                ->run()
            ;
        }

        $algorithm = $versionData['hash_algorithm'];
        $requiredHash = $versionData['hash'];
        $contents = file_get_contents($file);
        Assert::string($contents);
        $currentHash = hash($algorithm, $contents);
        if ($requiredHash === $currentHash) {
            $this->printTaskDebug("    $algorithm hash for file <info>$file</info> is <comment>$currentHash</comment> and matches");

            return true;
        }

        if ($this->listAlreadyRefreshed && isset($this->filesRefreshed[$file])) {
            throw new TaskException($this, <<<ERROR
$algorithm hash for file $file in version {$versionData['version']} should be
  $requiredHash, but is 
  $currentHash

ERROR
);
        }

        $this->printTaskWarning(<<<WARNING
Hash for file $file should be
  <info>$requiredHash</info>, but is
  <comment>$currentHash</comment>
Deleting file and reloading list.
WARNING
);
        unlink($file);
        $this->fetchAvailableModulesFile();
        $this->filesRefreshed[$file] = true;

        return false;
    }

    /**
     * Create Windows batch file to directly execute the given PHP/PHAR file.
     *
     * @param string $filename
     */
    private function createBatFile($filename)
    {
        $batPath = $this->binDir.'/'.$filename.'.bat';
        $this->printTaskDebug("    Checking for .bat file for <info>$filename</info>: <comment>$batPath</comment>");

        if (!file_exists($batPath) && false === strpos($filename, '.pubkey')) {
            $this->printTaskDebug("    <info>$batPath</info> does not exist, creating ...");
            $this->collectionBuilder()
                ->taskWriteToFile($batPath)
                ->line('@ECHO OFF')
                ->line("php %~dp0{$filename} %*")
                ->run()
            ;
        }
    }

    /**
     * @return array
     *
     * @throws TaskException
     */
    private function getAvailableModules()
    {
        if (!file_exists($this->availableModulesFile)) {
            if ($this->listAlreadyRefreshed) {
                throw new TaskException($this, 'Cannot load '.$this->availableModulesFile);
            }
            $this->fetchAvailableModulesFile();
        }

        $contents = file_get_contents($this->availableModulesFile);
        Assert::string($contents);
        $available = json_decode($contents, true);
        if (!is_array($available)) {
            throw new TaskException($this, 'Available modules file '.$this->availableModulesFile.' does not contain valid json data');
        }

        return $available;
    }

    /**
     * @throws TaskException
     */
    private function fetchAvailableModulesFile()
    {
        if ($this->listAlreadyRefreshed) {
            return;
        }

        $this->collectionBuilder()
            ->taskCiProviderRefreshModulesFile()
            ->run()
        ;

        $this->listAlreadyRefreshed = true;
    }

    /**
     * @return Result
     *
     * @throws TaskException
     */
    public function run()
    {
        if ($this->forceRefreshList) {
            $this->fetchAvailableModulesFile();
        }
        $this->checkModules();

        $message = 'All required CI modules are available, file hashes have been checked';
        $this->printTaskSuccess($message);

        return new Result($this, 0, $message);
    }
}
