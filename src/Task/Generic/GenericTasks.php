<?php

namespace C33s\Robo\Task\Generic;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Robo\Robo;

trait GenericTasks
{
    /**
     * Ask a confirm() question but only if in interactive mode.
     * In non-interactive input environments this method will always return true.
     *
     * @param string $question
     *
     * @return bool
     */
    protected function confirmIfInteractive($question)
    {
        return !$this->isInteractive() || $this->confirm($question);
    }

    /**
     * Ask a confirm() question but only if in interactive mode. Abort if not confirmed.
     *
     * @param $question
     */
    protected function confirmIfInteractiveOrAbort($question)
    {
        if (!$this->confirmIfInteractive($question)) {
            $this->abort();
        }
    }

    /**
     * Check if the current mode is interactive.
     *
     * @return bool
     */
    protected function isInteractive()
    {
        return $this->input()->isInteractive();
    }

    /**
     * Helper to create an OS specific Environment Variable export/set command.
     *
     * @param string $name
     * @param string $value
     *
     * @return string
     */
    protected function getExportCommand($name, $value) //TODO: php7 -  string
    {
        $name = strtoupper($name);

        if ($this->isWin()) {
            $pathExportCommand = "set {$name}={$value}";
        } else {
            $pathExportCommand = "export {$name}={$value}";
        }

        return $pathExportCommand;
    }

    /**
     * Check if the current Environment is CI.
     *
     * @return bool
     */
    protected function isEnvironmentCi() //TODO: php7 -  bool
    {
        if ('true' == getenv('CI') || '1' == getenv('CI')) {
            return true;
        }
        if (false !== getenv('CI_BUILD_TOKEN')) {
            return true;
        }
        if (false !== getenv('CI_JOB_ID')) {
            return true;
        }

        return false;
    }

    /**
     * Check if the current Environment is production.
     *
     * @return bool
     */
    protected function isEnvironmentProduction() //TODO: php7 - : bool
    {
        if ('dev' == strtolower(getenv('ENVIRONMENT'))) {
            return false;
        }
        if ('development' == strtolower(getenv('ENVIRONMENT'))) {
            return false;
        }
        if ($this->isEnvironmentCi()) {
            return false;
        }

        return true;
    }

    /**
     * Check if the current Environment is Development.
     *
     * @return bool
     */
    protected function isEnvironmentDevelopment() //TODO: php7 - : bool
    {
        return !$this->isEnvironmentProduction();
    }

    /**
     * Checks if the System is Windows or not.
     *
     * @return bool
     */
    protected function isWin()
    {
        if ('WIN' === strtoupper(substr(PHP_OS, 0, 3))) {
            return true;
        }

        return false;
    }

    /**
     * Check if the given port number is in use on the given host by trying to connect.
     *
     * @param int    $port
     * @param string $host
     *
     * @return bool
     */
    protected function isPortUsed($port, $host = '127.0.0.1')
    {
        $fp = @fsockopen($host, $port, $errno, $errstr, 5);

        if (!$fp) {
            return false;
        }
        fclose($fp);

        return true;
    }

    /**
     * Abort execution with the given message and exit code.
     *
     * @param string $message
     * @param int    $exitCode
     * @param string $wrapperTag
     */
    protected function abort($message = 'Aborting', $exitCode = 1, $wrapperTag = 'error')
    {
        if (!empty($wrapperTag)) {
            $message = "<$wrapperTag>$message</$wrapperTag>";
        }
        $this->say($message);

        exit($exitCode);
    }

    /**
     * Get the robo instance logger.
     *
     * @return LoggerInterface
     */
    protected function getLogger() //TODO: php7 -: LoggerInterface
    {
        try {
            return Robo::getContainer()->get('logger');
        } catch (NotFoundExceptionInterface $e) {
            return new NullLogger();
        } catch (ContainerExceptionInterface $e) {
            return new NullLogger();
        }
    }
}
