<?php

namespace C33s\Robo;

abstract class BaseRoboFile extends \Robo\Tasks implements \C33s\Robo\DefaultCi
{
    /**
     * returns the current configured directory for all robo vendors.
     *
     * @return string
     */
    protected function dir()
    {
        return C33S_ROBO_DIR;
    }
}
